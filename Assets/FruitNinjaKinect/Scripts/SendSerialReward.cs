using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.Threading;
using Setting;
using UnityEngine.Events;
using UnityEngine.UI;

public class SendSerialReward : MonoBehaviour
{
	[SerializeField] private SerialPortUtility.SerialPortUtilityPro _serialPort = null;
    [SerializeField] private GameObject _displayField;
    [SerializeField] private Transform _parent;
    [SerializeField] private int _numberOfChild;
    [SerializeField] private SelectButtonController _selectButtonControllerPrefab;
    [SerializeField] private Text _scoreText;
    [SerializeField] private GameObject notEnoughtText;

    public UnityAction StartPlaying;
    
   // private SerialPort _port;
    private RewardConfig _rewardConfig;
    private List<SelectButtonController> _selectButtonControllers;
    private int _currentScore;
    private string receivedData = string.Empty;
    private bool isRunning = false;
    private Thread readThread;

    // Invoked when a line of data is received from the serial device.
    void OnMessageArrived(string msg)
    {
        Debug.Log("Arrived: " + msg);
    }
    // Invoked when a connect/disconnect event occurs. The parameter 'success'
    // will be 'true' upon connection, and 'false' upon disconnection or
    // failure to connect.
    void OnConnectionEvent(bool success)
    {
        Debug.Log(success ? "Device connected" : "Device disconnected");
    }
    
    public void setRewardConfig(RewardConfig config)
    {
        this._rewardConfig = config;
    }

    public void SetScore(int score)
    {
        notEnoughtText.SetActive(false);
        this._currentScore = score;
        _scoreText.text = score.ToString();
        bool isAnyPrize = false;
        foreach (var scoreMark in this._rewardConfig.prizes)
        {
            for (int index = scoreMark.begin - 1; index <= scoreMark.end - 1; index++)
            {
                if (score >= scoreMark.score)
                {
                    isAnyPrize = true;
                    _selectButtonControllers[index].gameObject.SetActive(true);;
                }
                else
                {
                    _selectButtonControllers[index].gameObject.SetActive(false);;
                }
               
            }
        }
        _displayField.SetActive(true);
        if (!isAnyPrize)
        {
            notEnoughtText.SetActive(true);
            StartCoroutine(WaitForClose());
        }
    }

    private IEnumerator WaitForClose()
    {
        yield return new WaitForSecondsRealtime(3);
        _displayField.SetActive(false);
    }

    private void Start()
    {
        _selectButtonControllers = new List<SelectButtonController>();
   
        for (int index = 1 ; index <= _numberOfChild ; index ++)
        {
            SelectButtonController controller = Instantiate(_selectButtonControllerPrefab, _parent);
            controller.SetIndex(index,OnItemClick);
            _selectButtonControllers.Add(controller);
        }
    }

    void ReadSerialPort()
    {
    }
    
    public void SetReceiveString(object data){
		string receiveStr = data as string;
		receivedData = receiveStr;
	}

    public void Send(string message)
    {
        if (_serialPort.IsOpened())
        {
            _serialPort.SendMessage(message);

        }
        /* if (_port != null && _port.IsOpen)
         {
             try
             {
                 _port.WriteLine(message);
                 Debug.Log("Sent: " + message);
             }
             catch (System.Exception ex)
             {
                 Debug.LogError("Error sending to serial port: " + ex.Message);
             }
         }
         else
         {
             Debug.LogWarning("Serial port is not open.");
         }
     */
    }




    private void FixedUpdate()
    {
        if (!string.IsNullOrEmpty(receivedData))
        {
            Debug.Log("Received: " + receivedData);
            if (receivedData.Contains("game-start"))
            {
                StartPlaying?.Invoke();
            }
            receivedData = string.Empty;  // Clear the received data after processing
        }
    }

    private void OnItemClick(int index)
     {
         Debug.Log("SendRewardItem "+index);
          Send(index.ToString("D2"));
         _displayField.SetActive(false);
     }
     
     
     private void OnDestroy()
     {
		_serialPort.Close();
       /*  if (_port != null && _port.IsOpen)
         {
             _port.Close();
         }
		*/
     }
}
