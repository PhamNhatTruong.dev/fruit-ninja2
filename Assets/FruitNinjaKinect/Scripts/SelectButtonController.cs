using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class SelectButtonController : MonoBehaviour
{
    [SerializeField] private TMP_Text _displayText;
    
    private int _index;
    private UnityAction<int> _onClickAction;
    
    public void SetIndex(int index, UnityAction<int> onClick)
    {
        _index = index;
        _onClickAction = onClick;
        _displayText.text = index.ToString();
    }

    public void OnClick()
    {
        _onClickAction.Invoke(_index);
        Debug.Log("onKey down "+_index);
    }
}
