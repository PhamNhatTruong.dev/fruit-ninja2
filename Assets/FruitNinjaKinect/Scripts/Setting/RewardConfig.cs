﻿using System.Collections.Generic;


namespace Setting
{
    [System.Serializable]
    public class ScoreMark
    {
        public int begin { get; set; }
        public int end { get; set; }
        public int score { get; set; }
    }
    
    [System.Serializable]
    public class RewardConfig
    { 
        public string port { get; set; }
        public List<ScoreMark> prizes { get; set; }    
    }
}