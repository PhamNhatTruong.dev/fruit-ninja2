﻿namespace Setting
{
    public class KinectManagerConfig
    {
        public float maxUserDistance;
        public float minUserDistance;
        public float maxSideDistance;
        public float sensorHeight;
        public bool displayerUserMap;
        public bool displayerColorMap;
        public float displayMapWidthPercent;
        public bool enableDebugText;
    }
}